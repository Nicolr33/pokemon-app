package com.example.myapplication.services;

import android.net.Uri;
import android.util.Log;

import com.example.myapplication.models.Pokemon;
import com.example.myapplication.utils.HttpUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class PokemonService {
    private final String BASE_URL = "http://192.168.1.40:8000/api/";

    public ArrayList<Pokemon> getPokemons() {
        Uri uri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("pokemon")
                .build();
        String url = uri.toString();
        Log.d("XXXXXXXXXXXXXXXXX", url);

        return doCall(url);
    }

    private ArrayList<Pokemon> doCall(String url) {
        try {
            String response = HttpUtils.get(url);
            return process(response);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    private ArrayList<Pokemon> process(String jsonResponse) {
        ArrayList<Pokemon> pokelist = new ArrayList<>();
        try {
            JSONObject data = new JSONObject(jsonResponse);
            JSONArray jsonMovies = data.getJSONArray("results");
            for (int i = 0; i < jsonMovies.length(); i++) {
                JSONObject jsonPokemon = jsonMovies.getJSONObject(i);

                Pokemon pokeonly = new Pokemon();
                pokeonly.setName(jsonPokemon.getString("name"));
                pokeonly.setImg(jsonPokemon.getString("img"));
                pokeonly.setType0(jsonPokemon.getString("type0"));
                pokeonly.setType1(jsonPokemon.getString("type1"));
                pokeonly.setHeight(jsonPokemon.getString("height"));
                pokeonly.setWeight(jsonPokemon.getString("weight"));


                pokelist.add(pokeonly);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return pokelist;
    }

}
