package com.example.myapplication.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class Pokemon implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private float num;
    private String name;
    private String img;
    private String type0;
    private String type1;
    private String height;
    private String weight;
    private float next_evolution0num;
    private String next_evolution0name;
    private String prev_evolution0num = null;
    private String prev_evolution0name = null;


    // Getter Methods

    public int getId() {
        return id;
    }

    public float getNum() {
        return num;
    }

    public String getName() {
        return name;
    }

    public String getImg() {
        return img;
    }

    public String getType0() {
        return type0;
    }

    public String getType1() {
        return type1;
    }

    public String getHeight() {
        return height;
    }

    public String getWeight() {
        return weight;
    }

    public float getNext_evolution0num() {
        return next_evolution0num;
    }

    public String getNext_evolution0name() {
        return next_evolution0name;
    }

    public String getPrev_evolution0num() {
        return prev_evolution0num;
    }

    public String getPrev_evolution0name() {
        return prev_evolution0name;
    }

    // Setter Methods

    public void setId(int id) {
        this.id = id;
    }

    public void setNum(float num) {
        this.num = num;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public void setType0(String type0) {
        this.type0 = type0;
    }

    public void setType1(String type1) {
        this.type1 = type1;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public void setNext_evolution0num(float next_evolution0num) {
        this.next_evolution0num = next_evolution0num;
    }

    public void setNext_evolution0name(String next_evolution0name) {
        this.next_evolution0name = next_evolution0name;
    }

    public void setPrev_evolution0num(String prev_evolution0num) {
        this.prev_evolution0num = prev_evolution0num;
    }

    public void setPrev_evolution0name(String prev_evolution0name) {
        this.prev_evolution0name = prev_evolution0name;
    }
}
