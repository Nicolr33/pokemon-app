package com.example.myapplication.adapters;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.myapplication.R;
import com.example.myapplication.models.Pokemon;

import com.example.myapplication.databinding.PokemonRowBinding;
import java.util.List;


public class PokemonAdapter extends ArrayAdapter<Pokemon> {
    public PokemonAdapter(Context context, int resource, List<Pokemon> cards) {
        super(context, resource, cards);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Pokemon pokemon = getItem(position);

        PokemonRowBinding binding = null;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            binding = DataBindingUtil.inflate(inflater, R.layout.pokemon_row, parent, false);
        } else {
            binding = DataBindingUtil.getBinding(convertView);
        }


        binding.pokeName.setText(pokemon.getName());
        binding.pokeType0.setText(pokemon.getType0());
        Glide.with(getContext()).load(
                pokemon.getImg()
        ).apply(new RequestOptions().override(600, 200)).into(binding.pokeImage);


        return binding.getRoot();

    }
}
