package com.example.myapplication.ui.main;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.myapplication.dao.AppDatabase;
import com.example.myapplication.dao.PokemonDao;
import com.example.myapplication.models.Pokemon;
import com.example.myapplication.services.PokemonService;

import java.util.ArrayList;
import java.util.List;

public class MainViewModel extends AndroidViewModel {
    private final Application app;
    private final AppDatabase appDatabase;
    private final PokemonDao pokemonDao;

    public MainViewModel(Application application) {
        super(application);

        this.app = application;
        this.appDatabase = AppDatabase.getDatabase(
                this.getApplication());
        this.pokemonDao = appDatabase.getPokemonDao();
    }

    public LiveData<List<Pokemon>> getPokemons() {
        return pokemonDao.getPokemon();
    }

    public void reload() {
        RefreshDataTask task = new RefreshDataTask();
        task.execute();
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Pokemon>> {
        @Override
        protected ArrayList<Pokemon> doInBackground(Void... voids) {

            System.out.println("asd");

            PokemonService api = new PokemonService();
            ArrayList<Pokemon> result;

            result = api.getPokemons();

            pokemonDao.deletePokemons();
            pokemonDao.addPokemons(result);


            return result;
        }

        @Override
        protected void onPostExecute(ArrayList<Pokemon> pokemons) {
            super.onPostExecute(pokemons);
        }

    }
}
