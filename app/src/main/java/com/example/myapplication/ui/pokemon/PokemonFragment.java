package com.example.myapplication.ui.pokemon;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.myapplication.databinding.FragmentPokemonBinding;


import com.bumptech.glide.Glide;
import com.example.myapplication.models.Pokemon;
import com.example.myapplication.ui.SharedViewModel;

/**
 * A placeholder fragment containing a simple view.
 */
public class PokemonFragment extends Fragment {
    private View view;
    private FragmentPokemonBinding binding;

    public PokemonFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentPokemonBinding.inflate(inflater);
        view = binding.getRoot();


        Intent i = getActivity().getIntent();

        if (i != null) {
            Pokemon pokemon = (Pokemon) i.getSerializableExtra("pokemon");

            if (pokemon != null) {
                updateUi(pokemon);
            }
        }

        SharedViewModel sharedModel = ViewModelProviders.of(
                getActivity()
        ).get(SharedViewModel.class);

        sharedModel.getSelected().observe(this, pokemon -> updateUi(pokemon));
        return view;
    }

    @SuppressLint("SetTextI18n")
    private void updateUi(Pokemon pokemon) {

        binding.pokemonName.setText(pokemon.getName());
        binding.pokemonType1.setText(pokemon.getType0());


        if(pokemon.getType1().equals("null")) {
            binding.pokemonType2.setText("No tiene tipo 2");
        } else {
            binding.pokemonType2.setText(pokemon.getType1());
        }

        binding.pokemonHeight.setText(pokemon.getHeight());
        binding.pokemonWeight.setText(pokemon.getWeight());


        Glide.with(getContext()).load(pokemon.getImg()).into(binding.pokemonImage);
    }
}
