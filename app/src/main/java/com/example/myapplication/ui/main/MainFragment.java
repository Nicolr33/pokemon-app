package com.example.myapplication.ui.main;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.myapplication.R;
import com.example.myapplication.adapters.PokemonAdapter;
import com.example.myapplication.models.Pokemon;
import com.example.myapplication.services.PokemonService;
import com.example.myapplication.ui.SharedViewModel;
import com.example.myapplication.ui.pokemon.PokemonDetails;
import com.example.myapplication.databinding.MainFragmentBinding;

import java.util.ArrayList;
import java.util.List;

public class MainFragment extends Fragment {

    private ArrayList<Pokemon> items;
    private MainFragmentBinding binding;
    private SharedViewModel sharedViewModel;
    private PokemonAdapter adapter;
    private MainViewModel model;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void onStart() {
        super.onStart();
        refresh();
    }

    private void refresh() {
        model.reload();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {


        binding = MainFragmentBinding.inflate(inflater);
        View view = binding.getRoot();

        items = new ArrayList<>();

        adapter = new PokemonAdapter(getContext(), R.layout.pokemon_row, items);
        sharedViewModel = ViewModelProviders.of(getActivity()).get(
                SharedViewModel.class
        );

        binding.listPokemon.setAdapter(adapter);

        binding.listPokemon.setOnItemClickListener((adapter, fragment, i, l) -> {
            Pokemon pokemon = (Pokemon) adapter.getItemAtPosition(i);
            if (!esTablet()) {
                Intent intent = new Intent(getContext(), PokemonDetails.class);
                intent.putExtra("pokemon", pokemon);
                startActivity(intent);
            } else {
                sharedViewModel.select(pokemon);
            }
        });

        model = ViewModelProviders.of(this).get(MainViewModel.class);
        model.getPokemons().observe(this, pokemons -> {
            adapter.clear();
            adapter.addAll(pokemons);
        });


        return view;

    }

    boolean esTablet() {
        return getResources().getBoolean(R.bool.tablet);
    }



}
