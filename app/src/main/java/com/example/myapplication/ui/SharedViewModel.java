package com.example.myapplication.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.myapplication.models.Pokemon;

public class SharedViewModel extends ViewModel {
    private final MutableLiveData<Pokemon> selected = new MutableLiveData<Pokemon>();

    public void select(Pokemon movie) {
        selected.setValue(movie);
    }

    public LiveData<Pokemon> getSelected() {
        return selected;
    }
}